class Bucket < ApplicationRecord
    has_many :tasks
    belongs_to :user

    validate :owner_cannot_changed

    def self.get_by_status(status)
        self.all.select do |bucket|
            bucket.status == status
        end
    end

    def update_status
        if self.tasks == []
            self.status = "Empty"
        elsif self.tasks.any?{|task| task.status == "Pending"}
            self.status = "Pending"
        else
            self.status = "Completed"
        end
        self.save
    end

    def tasks_by_status(status)
        self.tasks.select do |task|
            task.status == status
        end
    end

    def owner_cannot_changed
        if self.persisted? && self.user_id_changed?
            errors.add(:owner, "can not be changed.")
        end
    end
end
