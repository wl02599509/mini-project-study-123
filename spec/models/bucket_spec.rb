require 'rails_helper'

RSpec.describe Bucket, type: :model do
  let(:user) { create(:user) }
  let(:bucket) { create(:bucket) }
  let(:task) { create(:task) }

  it "has_many tasks" do
    task = bucket.tasks.create(:name=>"name", :description=>"description", :status => "Pending")
    expect(task.bucket_id).to eq(bucket.id)
  end

  it "belongs_to user" do
    new_bucket = user.buckets.create(:name=>"name", :description=>"description")
    expect(new_bucket.user_id).to eq(user.id)
  end

  it "owner_cannot_changed" do
    bucket.update(:user_id => 2)
    expect(bucket.user_id_changed?).to be true
    expect(bucket.errors.full_messages).to include("Owner can not be changed.")
  end

  it ".get_by_status('Empty')" do
    bucket_with_empty_status = bucket
    expect(Bucket.get_by_status('Empty')).to include bucket_with_empty_status
  end
  
  it "#update_status pengind / .get_by_status('Pending')" do
    bucket.tasks.create(:name=>"name", :description=>"description", :status => "Pending")
    bucket.update_status
    expect(bucket.previous_changes).to include {"status"=>["Empty", "Pending"]}
    expect(Bucket.get_by_status('Pending')).to include bucket
  end

  it "#update_status completed / .get_by_status('Completed')" do
    bucket.tasks.create(:name=>"name", :description=>"description", :status => "Completed")
    bucket.update_status
    expect(bucket.previous_changes).to include {"status"=>["Pending", "Completed"]}
    expect(Bucket.get_by_status('Completed')).to include bucket
  end

  it "#tasks_by_status('Pending')" do
    task = Task.create(:name=>"name", :description=>"description", :status => "Pending")
    task.bucket = bucket
    task.save
    expect(bucket.tasks_by_status('Pending')).to include task
  end

  it "#tasks_by_status('Completed')" do
    task = Task.create(:name=>"name", :description=>"description", :status => "Completed")
    task.bucket = bucket
    task.save
    expect(bucket.tasks_by_status('Completed')).to include task
  end
end
