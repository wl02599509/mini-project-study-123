require 'rails_helper'

RSpec.describe Task, type: :model do
  let(:bucket) { create(:bucket) }

  it ".get_by_status('Pending)" do
    task = bucket.tasks.create(:name=>"name", :description=>"description", :status => "Pending")
    expect(Task.get_by_status('Pending')).to include task
  end

  it ".get_by_status('Completed')" do
    task = bucket.tasks.create(:name=>"name", :description=>"description", :status => "Completed")
    expect(Task.get_by_status('Completed')).to include task
  end
end
