require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { create(:user) }

  it 'has_many buckets' do
    bucket = user.buckets.create(:name=>"name", :description=>"description", :status => "Empty")
    expect(user.buckets).to include bucket
  end
end
