require 'rails_helper'

RSpec.describe TasksController, type: :controller do
  let(:user) { create(:user) }
  let(:bucket) { user.buckets.create(:name=>"name", :description=>"description") }
  let(:task) { bucket.tasks.create(:name=>"name", :description=>"description") }

  it "#index" do
    sign_in user
    get :index
    expect(response).to have_http_status(200)
    expect(response).to render_template(:index)
  end

  it "#new" do
    sign_in user
    get :new
    expect(response).to have_http_status(200)
    expect(response).to render_template(:new)
  end

  describe "#create" do
    before do
      sign_in user
      post :create, :params => { :task => { :name=>"name", :description=>"description", :bucket_id => bucket.id } }
    end

    it "create record" do
      expect(Task.all.size).to eq 1
    end

    it "new task status" do
      expect(Task.last.status).to eq "Pending"
    end

    it "redirect_to task_path" do
      expect(response).to redirect_to(task_path(Task.last))
    end
  end

  it "#show" do
    sign_in user
    get :show, params:{ id: task.id }
    expect(response).to have_http_status(200)
  end

  it "#edit" do
    sign_in user
    get :edit, params: { id: task.id }
    expect(response).to have_http_status(200)
    expect(response).to render_template(:edit)
  end

  describe "#update" do
    before do
      sign_in user
      put :update, :params => { :id => task.id, :task => { :name=>"name_changed", :description=>"description" }, :bucket_id => bucket.id }
    end

    it "update record" do
      expect(task.previous_changes).to include {:name => ["name", "name_changed"]}
    end

    it "redirect_to task_path" do
      expect(response).to redirect_to(task_path(task))
    end
  end
  
  it "#destroy" do
    sign_in user
    new_task = bucket.tasks.create(:name=>"name", :description=>"description")
    expect(Task.count).to eq 1
    delete :destroy, params: {id: new_task.id}
    expect(Task.count).to eq 0
  end
end
