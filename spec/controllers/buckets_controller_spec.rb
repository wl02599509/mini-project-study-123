require 'rails_helper'

RSpec.describe BucketsController, type: :controller do
  let(:user) { create(:user) }
  let(:bucket) { user.buckets.create(:name=>"name", :description=>"description") }

  it "#index" do
    sign_in user
    get :index
    expect(response).to have_http_status(200)
    expect(response).to render_template(:index)
  end

  it "#new" do
    sign_in user
    get :new
    expect(response).to have_http_status(200)
    expect(response).to render_template(:new)
  end

  describe "#create" do
    before do
      sign_in user
      post :create, :params => { :bucket => { :name=>"name", :description=>"description" } }
    end

    it "create record" do
      expect(Bucket.all.size).to eq 1
    end

    it "new bucket status" do
      expect(Bucket.last.status).to eq "Empty"
    end

    it "redirect_to bucket_path" do
      expect(response).to redirect_to(bucket_path(Bucket.last))
    end
  end

  it "#show" do
    sign_in user
    get :show, params:{ id: bucket.id }
    expect(response).to have_http_status(200)
  end

  it "#edit" do
    sign_in user
    get :edit, params: { id: bucket.id }
    expect(response).to have_http_status(200)
    expect(response).to render_template(:edit)
  end

  describe "#update" do
    before do
      sign_in user
      put :update, :params => { :id => bucket.id, :bucket => { :name=>"name_changed", :description=>"description" } }
    end

    it "update record" do
      expect(bucket.previous_changes).to include {:name => ["name", "name_changed"]}
    end

    it "redirect_to bucket_path" do
      expect(response).to redirect_to(bucket_path(bucket))
    end
  end
  
  it "#destroy" do
    sign_in user
    new_bucket = user.buckets.create(:name=>"name", :description=>"description")
    expect(Bucket.count).to eq 1
    delete :destroy, params: {id: new_bucket.id}
    expect(Bucket.count).to eq 0
  end
end
