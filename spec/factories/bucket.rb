FactoryBot.define do
  factory :bucket do
    user 
    name { "Study123" }
    description { "I love learning." }
    status { "Empty" }
  end
end
