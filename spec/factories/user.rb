require 'securerandom'

FactoryBot.define do
  factory :user do
    email { "mike@mike.com" }
    password { SecureRandom.hex(10) }
  end
end
